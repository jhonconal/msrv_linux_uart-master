TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    msrv_linux_uart.cpp

HEADERS += \
    msrv_linux_uart.h

