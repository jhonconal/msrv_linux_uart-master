
SOURCES= main.cpp msrv_linux_uart.cpp 

LINUX_SRC = $(SOURCES)
LINUX_APP = msrv_linux_uart
CC=g++
#CC=aarch64-linux-gnu-g++

all: $(LINUX_APP)

$(LINUX_APP): $(LINUX_SRC)
	$(CC) -pthread $^ -o $@ $(LDFLAGS) -static

install: all

clean:
	@rm -f $(LINUX_APP)

check:
